\chapter{Automated Testing}
\label{ch:testing}

The build system of the framework provides a set of automated tests which are executed by the CI to ensure proper functioning of the framework and its modules.
The tests can also be manually invoked from the build directory of \apsq with
\begin{verbatim}
$ ctest
\end{verbatim}
When executed by the CI, the results on passed and failed tests are automatically gathered and prominently displayed in merge requests along with the overall CI pipeline status.
This allows a quick identification of issues without having to manually search through the log of several CI jobs.

The different subcategories of tests described below can be executed or ignored using the \command{-E} (exclude) and \command{-R} (run) switches of the \command{ctest} program:
\begin{verbatim}
$ ctest -R test_performance
\end{verbatim}

A list of all tests performed with this version of \apsq is provided in Appendix~\ref{app:testlist}.
\section{Test Configurations}

Test configuration files consist of regular \apsq configuration files for a simulation, invoking the desired behavior to be tested.
In addition, test files can contain tags and pass conditions as described in the subsequent section.

CMake automatically searches for \apsq configuration files in the different directories and passes them to the \apsq executable~(cf.\ Section~\ref{sec:allpix_executable}).
Adding a new test is as simple as adding a new configuration file to the respective directories and specifying the pass or fail conditions based on the tags described in the following paragraphs.

Three different types of tests are distinguished:

\paragraph{Framework Functionality Tests}

The framework functionality tests validate the core framework components such as seed distribution, multithreading capabilities, configuration parsing and coordinate transformations. The configuration of the tests can be found in the \dir{etc/unittests/test_core} directory of the repository and are automatically discovered by CMake.

\paragraph{Module Functionality Tests}

These tests are meant to ensure proper functioning of an individual module given a defined input and thus protect the framework against accidental changes affecting the physics simulation.
Using a fixed seed (using the \parameter{random_seed} configuration keyword) together with a specific version of Geant4~\cite{geant4}, if necessary, allows to reproduce the same simulation event.

Typically, one single event is produced per test and the \parameter{DEBUG}-level logging output of the respective module is checked against pre-defined expectation output using regular expressions.
Once modules are altered, their respective expectation output has to be adapted after careful verification of the simulation result.

Module tests are located within the individual module source folders and are only enabled if the respective module will be built. For new modules, the directory in which the test files are located needs to be registered in the main CMake file of the module as described in Section~\ref{sec:module_files}. Module test files have to start with a two-digit number and end with the file extension \parameter{.conf}, e.g. \parameter{01-mytest.conf}, to be detected.

\paragraph{Performance Tests}

These tests run a set of simulations on a dedicated machine to catch any unexpected prolongation of the simulation time, e.g. by an accidentally introduced heavy operation in a hot loop.
Performance tests use configurations prepared such, that one particular module takes most of the load (dubbed the \emph{slowest instantiation} by \apsq), and a few of thousand events are simulated starting from a fixed seed for the pseudo-random number generator.
The \parameter{#TIMEOUT} keyword in the configuration file will ask CTest to abort the test after the given running time.

In the project CI, performance tests are limited to native runners, i.e. they are not executed on docker hosts where the hypervisor decides on the number of parallel jobs.
Only one test is performed at a time.

Despite these countermeasures, fluctuations on the CI runners occur, arising from different loads of the executing machines.
Thus, all performance CI jobs are marked with the \parameter{allow_failure} keyword which allows GitLab to continue processing the pipeline but will mark the final pipeline result as \emph{passed with warnings} indicating an issue in the pipeline.
These tests should be checked manually before merging the code under review.

The configuration of the tests can be found in the \dir{etc/unittests/test_performance} directory of the repository and are automatically discovered by CMake.

\section{Test Tags, Pass and Fail Conditions}

Test tags allow to influence the execution condition of the given test configuration, or to define a required condition for passing or failing the test.
These expressions are simply placed in the configuration file of the corresponding tests, a tag at the beginning of the line indicates which test tag the line corresponds to.
The following tags are available:

\begin{description}
  \item[Passing a test] The expression marked with the tag \parameter{#PASS} has to be found in the output in order for the test to pass. If the expression is not found, the test fails.
  \item[Failing a test] If the expression tagged with \parameter{#FAIL} is found in the output, the test fails. If the expression is not found, the test passes.
  \item[Depending on another test] The tag \parameter{#DEPENDS} can be used to indicate dependencies between tests. For example, module test \parameter{ROOTObjectReader/01-reading} implements such a dependency as it uses the output of module test \parameter{ROOTObjectWriter/01-write} to read data from a previously produced \apsq data file.
  \item[Defining a timeout] For performance tests the runtime of the application is monitored, and the test fails if it exceeds the number of seconds defined using the \parameter{#TIMEOUT} tag.
  \item[Adding additional CLI options] Additional module command line options can be specified for the \parameter{allpix} executable using the \parameter{#OPTION} tag, following the format found in Section~\ref{sec:allpix_executable}. The \parameter{-o} flag will be added automatically. Multiple options can be supplied by repeating the \parameter{#OPTION} tag in the configuration file, only one option per tag is allowed. In exactly the same way options for the detectors can be set as well using the \parameter{#DETOPION} tag, where \parameter{-g} will be added automatically.
  For all other command line options to be passed to the executable, the \parameter{#CLIOPTION} can be used. Here, the complete flag and possible value needs to be passed, e.g.\ \parameter{-j9}.
  \item[Defining a test case label] Tests can be grouped and executed based on labels, e.g.\ for code coverage reports. Labels can be assigned to individual tests using the \parameter{#LABEL} tag.
  \item[Describing the test] Every test should bear a short description of its goal. This descriptive text can be provided via the \parameter{#DESC} tag and is required for every test. CMake will print warnings for every test missing this tag.
  \item[Running scripts] Some tests require additional input which needs to be generated by a script. For this propose, commands to be executed \emph{before} the tests starts can be provided via the \parameter{#BEFORE_SCRIPT} tag, e.g.
  \begin{minted}[frame=single,framesep=3pt,breaklines=true,tabsize=2,linenos]{bash}
  #BEFORE_SCRIPT python @PROJECT_SOURCE_DIR@/etc/scripts/create_deposition_file.py --type a --detector mydetector --events 2 --steps 1 --seed 0
  \end{minted}
  to run a Python script that generates an input file read by the test.
  \item[Requiring external data] Some tests require external data which needs to be downloaded before executing the test. For this purpose, the \parameter{#DATA} tag is available, which can contain file paths which will be set as required files for the test. If registered with CMake's ExternalData module, they will be downloaded automatically.
\end{description}

\section{Interpretation of Pass and Fail Conditions}

Multiple pass or fail conditions can be separated by a semicolon or by adding multiple \parameter{#PASS} or \parameter{#FAIL} expressions.
It should however be noted that test passes or fails \emph{if any of these conditions is met}, i.e.\ the conditions are combined with a logical \parameter{OR}.
At least one pass or one fail conditions must be present in every test.

Pass and fail condition are not interpreted as regular expressions but relevant characters are automatically escaped.
This allows to directly copy corresponding lines form the log into the respective condition without manually creating a matching regular expression.
A noteworthy exception to this are line breaks.
To ease matching of multi-line expressions, the newline escape sequence \parameter{\n} of any test expression is automatically expanded to \parameter{[\r\n\t ]*} to match any new line, carriage return, tab and whitespace characters following the line break.

\section{Warning and Error Messages During Testing}

If no explicit fail conditions are specified, the test will fail if any \parameter{WARNING}, \parameter{ERROR} or \parameter{FATAL} appears in the output log unless it is already part of the pass condition.
For example, if a test is supposed to pass in case of an error provoked

\begin{minted}[frame=single,framesep=3pt,breaklines=true,tabsize=2,linenos]{bash}
(FATAL) [I:GeometryBuilderGeant4] Error during execution of run:
                                  Could not find a detector model of type 'missing_model'
                                  Please check your configuration and modules. Cannot continue.
\end{minted}

The full error message including the \parameter{FATAL} has to be provided as pass condition:

\begin{minted}[frame=single,framesep=3pt,breaklines=true,tabsize=2,linenos]{ini}
#PASS (FATAL) [I:GeometryBuilderGeant4] Error during execution of run:\nCould not find a detector model of type 'missing_model'
\end{minted}

If a test is expected to create multiple error or warning messages which cannot be matched with a single pass condition, the \parameter{#FAIL} parameter should be set explicitly to avoid matching the respective flags:

\begin{minted}[frame=single,framesep=3pt,breaklines=true,tabsize=2,linenos]{ini}
# This test created multiple WARNING messages, we exclude WARNING from the
# fail expression by explicitly defining it as FATAL only:
#PASS (ERROR) Multithreading disabled since the current module configuration does not support it
#FAIL FATAL
\end{minted}

\section{Directory Variables in Tests}

Sometimes it is necessary to pass directories or file names as test input.
To facilitate this, the test files can contain variables which are replaced with the respective paths before being executed.
All variable names have to be enclosed in \parameter{@} symbols to be detected and parsed correctly.
Variables can be used both in test files and the auxiliary configuration files such as detector geometry definitions.

The following variables are available:

\begin{description}
  \item[\parameter{@TEST_DIR@}:] Directory in which the current test is executed, i.e. where all output files will be placed.
  \item[\parameter{@TEST_BASE_DIR@}:] Base directory under which all tests are being executed. This can be used to reference the output files from another test. it should be noted that the respective test has to be referenced using the \parameter{#DEPENDS} keyword to ensure that it successfully ran before.
  \item[\parameter{@PROJECT_SOURCE_DIR@}:] The root directory of the project. This can for example be used to call a script provided in the \dir{etc/scripts} directory of the repository.
\end{description}

The following example demonstrates the use of these variables.
A script is called before executing the test and an input file is expected:

\begin{minted}[frame=single,framesep=3pt,breaklines=true,tabsize=2,linenos]{ini}
[Allpix]
detectors_file = "detector.conf"

[DepositionReader]
file_name = "@TEST_DIRECTORY@/deposition.root"

#BEFORE_SCRIPT python @PROJECT_SOURCE_DIR@/etc/scripts/create_deposition_file.py --type a --detector mydetector
\end{minted}

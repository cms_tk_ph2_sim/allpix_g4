#include <TFile.h>
#include <TTree.h>
#include <iostream>

int make_root()
{
	TFile *_save = new TFile("data.root", "NEW", "AnalysedData");
        TTree *_tree = new TTree("tree","Main");
        
	int idEvent;
	int pdg_code;
	float px;
	float py;
	float pz;
	float gx;
	float gy;
	float gz;
	float E;

	_tree->Branch("E", (&E));
	_tree->Branch("pdg", (&pdg_code));
	_tree->Branch("px", (&px));
	_tree->Branch("py", (&py));
	_tree->Branch("pz", (&pz));
	_tree->Branch("gx", (&gx));
	_tree->Branch("gy", (&gy));
	_tree->Branch("gz", (&gz));
	_tree->Branch("idEvent", (&idEvent));

	E = 1200;
	px = 0;
	py = 0;
	pz = 1;
	gx = 0;
	gy = 0;
	gz = 0;
	idEvent = 0;
	pdg_code = 13;

	_tree->Fill();

	E = 1000;
	px = 1;
	py = 1;
	pz = 1;
	gx = 0.5;
	gy = 0;
	gz = 0;
	idEvent = 1;
	pdg_code = 13;

	_tree->Fill();

	_save->cd();
	_tree->Write();
	_save->Close();
	return 0;
}
